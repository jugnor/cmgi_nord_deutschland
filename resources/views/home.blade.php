<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DEUTSCHLAND &mdash; </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
        <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
        <meta name="author" content="freehtml5.co" />

        <!-- 
        //////////////////////////////////////////////////////

        FREE HTML5 TEMPLATE 
        DESIGNED & DEVELOPED by FreeHTML5.co
                
        Website: 		http://freehtml5.co/
        Email: 			info@freehtml5.co
        Twitter: 		http://twitter.com/fh5co
        Facebook: 		https://www.facebook.com/fh5co

        //////////////////////////////////////////////////////
        -->

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700" rel="stylesheet">

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!-- Flexslider  -->
        <link rel="stylesheet" href="css/flexslider.css">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="css/style.css">

        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->


        <script src="js/readMore.js"></script>
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Flexslider -->
        <script src="js/jquery.flexslider-min.js"></script>
        <!-- Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Magnific Popup -->
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <!-- Counters -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Main -->
        <script src="js/main.js"></script>

    </head>
    <body>

        <div class="fh5co-loader"></div>

        <div id="page">
            <nav class="fh5co-nav" role="navigation">
                <div class="container-wrap">
                    <div class="top-menu">
                        <div class="row">
                            <div class="col-md-12 col-offset-0 text-center">
                                <div id="fh5co-logo"><a href="">CMGI DEUTSCHLAND</a></div>
                            </div>
                            <div class="col-md-12 col-md-offset-0 text-center menu-1">
                                <ul>
                                    <li class="active"><a href="/home">Hause</a></li>
                                    <li><a href="/sermons">Sermone</a></li>
                                    <li class="has-dropdown">
                                        <a href="/news">News</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Ministry</a></li>
                                            <li><a href="#">Medical Missionary</a></li>
                                            <li><a href="#">Country Living</a></li>
                                            <li><a href="#">Gospel Workers</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/events">Ereignisse</a></li>
                                    <li><a href="/about">Über uns</a></li>
                                    <li><a href="/contact">Kontakt</a></li>
                                    <li><a href="#" class="donate">Donate</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>
            <div class="container-wrap">
                <aside id="fh5co-hero">
                    <div class="flexslider">
                        <ul class="slides">
                            <li style="background-image: url(images/img_bg_1.jpg);">
                                <div class="overlay"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3 text-center">
                                            <div class="slider-text">
                                                <div class="slider-text-inner">
                                                <h3>CMGI FRANKFURT</h3>
                                                    <h1>Berner Straße 107, 60437 Frankfurt am Main</h1>
                                                    <h2>01622346204 / frankfurt@cmgi-online.de  <!--<a href="http://freehtml5.co/" target="_blank">freehtml5.co</a>--></h2>
                                                  
                                                    <p><a class="btn btn-primary btn-demo popup-vimeo" href="https://www.youtube.com/watch?v=wEW0H1Asfeg"> <i class="icon-play4"></i> Unsere letzte Gottesdienst</a> <!--<a class="btn btn-primary btn-learn">Join us here! <i class="icon-arrow-right3"></i></a>-->
                                                    <h4>Der Weg des christlichen Charackter</h4>
                                                
                                                </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style="background-image: url(images/img_bg_2.jpg);">
                                <div class="overlay"></div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3 text-center">
                                            <div class="slider-text">
                                                <div class="slider-text-inner">
                                                <h3>CMGI FRANKFURT</h3>
                                                    <h1>Sonntag Gottesdient 11:00-13:30</h1>
                                                    <h1>Dienstag Online Lehre 20:00-20:30: Die Bibel in 30 Minuten <!-- <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a>--></h1>
                                                    
                                                    <p><a class="btn btn-primary btn-demo popup-vimeo" href="https://www.youtube.com/watch?v=wEW0H1Asfeg"> <i class="icon-play4"></i> Unsere Letzte Gottesdienst</a> <!--<a class="btn btn-primary btn-learn">Join us here! <i class="icon-arrow-right3"></i></a>--></p>
                                                    <h4>Der Weg des christlichen Charackter</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style="background-image: url(images/img_bg_3.jpg);">
                                <div class="overlay"></div>
                                <div class="container-fluids">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3 text-center">
                                            <div class="slider-text">
                                                <div class="slider-text-inner text-center">
                                                <h3>CMGI FRANKFURT</h3>
                                                    <h1>Freitag Gebetsabend 19:30-23:00</h1>
                                                    <h1>Montag GebetsNacht für die Nation Eroberung 23:30-04:00<!--<a href="http://freehtml5.co/" target="_blank">freehtml5.co</a>--></h1>
                                                    <p><a class="btn btn-primary btn-demo popup-vimeo" href="https://www.youtube.com/watch?v=wEW0H1Asfeg"> <i class="icon-play4"></i> Unsere Letzte Gottesdienst</a> <!--<a class="btn btn-primary btn-learn">Join us here! <i class="icon-arrow-right3"></i></a>--></p>
                                                    <h4>Der Weg des christlichen Charackter</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </aside>




                <div id="fh5co-services" class="fh5co-light-grey">
                    <div class="row animate-box">
                        <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                            <h2>Unser Buchladen</h2>
                            <p>Endecken Sie in Unserer Galerie jede Woche 3 interessanten preiswerten Büchertiteln und melden Sie sich bei uns für eine Kopie </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 animate-box">
                            <div class="services">
                                <a href="#" class="img-holder"><img class="img-responsive" src="images/WL.jpg" alt="Free HTML5 Website Template by freehtml5.co"></a>
                                <div class="desc">
                                    <h3><a href="#">Der Weg des Lebens</a></h3>
                                    <p>Dieses Buch zeigt uns, wie es möglich wird, durch eine lebendige Beziehung mit Jesus, 
                                    den Weg zum christlichen Leben zu finden und Gemeinschaft mit Gott zu haben. In wenigen Worten, 
                                    spricht dieses Buch über den Weg, der zum Leben führt, nämlich  des Lebens“.
                                    <span id="dot1">...</span><span id="more1"> Es wurde besonders geschrieben, 
                                    um dir zu helfen, dem Herrn Jesus zu begegnen und ein Leben durchzuführen, das Er willkürlich jedem anbietet, 
                                    der sich zu Ihm wendet. Dieses Buch ist für dich. Lese es. Begegne dem Herrn Jesus. Gib dich für Ihn ganz hin 
                                    und du wirst erst dann anfangen, tatsächlich zu leben.</span></p>
                                    <a href='javascript:liebeUndVergebung1()' id ="myBtn1">Read More <i class="icon-arrow-right3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="services">
                                <a a href="#" class="img-holder"><img class="img-responsive" src="images/GL.png" alt="Free HTML5 Website Template by freehtml5.co"></a>
                                <div class="desc">
                                    <h3><a href="#">Gottes Liebe und Vergebung</a></h3>
                                    <p>Dieses Buch behandelt den Weg zur Erlösung, im Einzeln </br>
                                        - Gottes ursprüngliche Absicht in der Schöpfung des Menschen.</br>
                                        - Die tragische Sünde des Menschen und seine Konsequenzen in Zeit und Ewigkeit.</br>
                                        - Die unvergleichliche Liebe Gottes, ausgedruckt durch die erlösende Liebe Christi am Kreuz für den rebellischen Menschen.</br>
                                        <span id="dot2">...</span><span id="more2">- Die Antwort des Sünders an Gott durch Buße gegenüber Gott und Glauben an Jesus Christus.</br>
                                        Lies dieses Buch und erfahre auf eine neue Weise Gottes Liebe zu dir.
                                        Laß deine Freund, deinen Partner, deine Eltern, deine Kollegen, deine Nachbarn dieses Buch entdecken, und sie werden dadurch gesegnet werden.</p>
                                    <a href='javascript:liebeUndVergebung2()' id ="myBtn2">Read More <i class="icon-arrow-right3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="services">
                                <a href="#" class="img-holder"><img class="img-responsive" src="images/KN.jfif" alt="Free HTML5 Website Template by freehtml5.co"></a>
                                <div class="desc">
                                    <h3><a href="#">Keine Niederlage muss endgüldig sein</a></h3>
                                    <p>Der Herr hat einen wunderbaren Plan für dich. Er hat große Dinge beschlossen für dich.
                                     Unabhängig davon, was du warst und was du als Ungläubiger getan hast; unabhängig von den Sünden, 
                                     die du seit deiner Bekehrung begangen hast; unabhängig von deinem Versagen und deiner Untreue in der Vergangenheit; 
                                     unabhängig davon, dass du ihn in der Vergangenheit verleugnet hast; unabhängig von deiner Entmutigung in seinem Dienst;
                                     unabhängig von deiner Abkehr in der Vergangenheit; unabhängig von deiner geistigen Unfruchtbarkeit; 
                                     <span id="dot3">...</span><span id="more3">-unabhängig von dem, was Er gegen dich ausgesprochen hat ..., kannst du immer noch aufstehen und in seine Fülle eintreten,
                                      denn keine Niederlage der Vergangenheit oder der Gegenwart muss endgültig sein. Du kannst dich Ihm heute zuwenden 
                                      und Er wird dich empfangen und Sein großes Ziel für dich umsetzen. Er will jetzt beginnen. Gewähre es Ihm. Du sollst zu Ihm sagen:
                                       „Herr, hier bin ich. Setze dich durch. Herr, ermögliche, dass deine vollen Absichten, mir eine Zukunft und eine Hoffnung zu geben,
                                        in Erfüllung gehen.“ Dann wende dich von deiner Vergangenheit ab und ruhe in Seiner Liebe. Er wird dich nicht zurückweisen.
                                         Er wird dich empfangen und Er wird Seine glorreichen Absichten für dich erfolgreich umsetzen. Amen.</p>
                                         <a href='javascript:liebeUndVergebung3()' id ="myBtn3">Read More <i class="icon-arrow-right3"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            
                <div id="fh5co-bible-verse">
                    <div class="overlay"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row animate-box">
                                <div class="owl-carousel owl-carousel-fullwidth">
                                    <div class="item">
                                        <div class="bible-verse-slide active text-center">
                                            <blockquote>
                                                <p>&ldquo;For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.&rdquo;</p>
                                                <span>John 3:16</span>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="bible-verse-slide active text-center">
                                            <blockquote>
                                                <p>&ldquo;The LORD [is] my strength and my shield; my heart trusted in him, and I am helped: therefore my heart greatly rejoiceth; and with my song will I praise him.&rdquo;</p>
                                                <span>Psalms 28:7</span>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="bible-verse-slide active text-center">
                                            <blockquote>
                                                <p>&ldquo;And we have known and believed the love that God hath to us. God is love; and he that dwelleth in love dwelleth in God, and God in him.&rdquo;</p>
                                                <span>1 John 4:16</span>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               


                <div id="fh5co-intro">
                    <div class="row animate-box">
                        <div class="col-md-12 col-md-offset-0 text-center">
                            <h2>Unser Ziel in diesem Jahr 2020</h2>
                            <span>Wir wollen das Wort Gottes teilen und Hauskreise in Frankfurt und Co gründen.</br></span>
        
                            <span>Sie sind hierfür ganz herzlich willkommen in einer unsere Hauskreise</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="fh5co-counter" class="fh5co-counters">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center animate-box">
                            <p></p>
                        </div>
                    </div>
                    <div class="row animate-box">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="row">
                               < <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="30" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">Hauskreise</span>
                                </div>
                               
                                <div class="clearfix visible-sm-block visible-xs-block"></div>
                                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="50" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">von 10 bis 50 Mitglieder</span>
                                </div>
                                <div class="clearfix visible-sm-block visible-xs-block"></div>
                                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="250" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">Nationen mit Liebe Gottes erreichen</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                </div>
            </div><!-- END container-wrap -->

            <div class="container-wrap">
                <footer id="fh5co-footer" role="contentinfo">
                    <div class="col-md-4 text-center">
                        <h3>198 West 21th Street, Suite 721 New York NY 10016</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <h2><a href="#">Church</a></h2>
                    </div>
                    <div class="col-md-4 text-center">
                        <p>
                        <ul class="fh5co-social-icons">
                            <li><a href="#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="#"><i class="icon-dribbble2"></i></a></li>
                        </ul>
                        </p>
                    </div>
                    <div class="row copyright">
                        <div class="col-md-12 text-center">
                            <p>
                                <small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
                                <small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a></small>
                            </p>
                        </div>
                    </div>
                </footer>
            </div><!-- END container-wrap -->
        </div>

        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
        </div>

        

    </body>
</html>

